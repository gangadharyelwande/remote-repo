/**
 * 
 */
package com.retail.core;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author gyelwand
 *
 */
public class ShipmentCart {
	
	//Create the list of Items which stores items
	List<Item> items= new LinkedList<Item>();//code on interface
	private double totalShippingCost=0.00;
	
	//Constructor initialize list with default values
	public ShipmentCart() {
		items= new ArrayList<Item>();
	}
	
	//Add items in the list
	public void addItem(Item item) {
		this.items.add(item);
		this.setStrategy(item);
	}
	
	//set the strategy and calculate shipping cost
	public void setStrategy(Item item) {
		String shippingMethod =item.getShippingMethod();
		if(shippingMethod.equals("AIR")) {
			new AirShippingStrategy().calculateShippingCost(item);
		}
		else if(shippingMethod.equals("GROUND")){
			new GroundShippingStrategy().calculateShippingCost(item);
		}
		
		//calculate total shipping cost each time when new item gets added in list
		totalShippingCost+=item.getShippingCost();
		
		 DecimalFormat f = new DecimalFormat("##.##");
		 String totalShippingCostStr=f.format(totalShippingCost);
		 
		 totalShippingCost=Double.parseDouble(totalShippingCostStr);
		
	}
	
	//display Items in the specified format
	public void display() {
		LocalDateTime today = LocalDateTime.now();

		System.out.print("\n****SHIPMENT REPORT****");
		System.out.print(String.format("%50s%-50s","Date:",today));
		System.out.println("\n");
		System.out.print(String.format("%-20s%-45s%-11s%-11s%-20s%-20s","UPC","Description","Price","Weight","Ship Method",
										"Shipping Cost"));
		System.out.println("\n");
		
		for(Item item:items) {
			System.out.format("%-20s%-45s%-11.2f%-11.2f%-20s%-20.2f\n",item.getUpc(),item.getDescription(),item.getPrice(),
								item.getWeight(),item.getShippingMethod(),item.getShippingCost());
		}
		System.out.println();
		System.out.print(String.format("%-30s", "TOTAL SHIPPING COST:"));
		System.out.print(String.format("%-40s%-11.2s%-11.2s%-15s%-11.2f\n", " "," "," "," ",totalShippingCost));
	}
	
	public void displayItemsWithColumnNameValuePair() {
		
		for(Item item:items) {
			System.out.println("upc="+item.getUpc()+",  description="+item.getDescription()
					          +",   price="+item.getPrice()+",   weight="+
								item.getWeight()+",   shippingType="+item.getShippingMethod()+",   shippingCost="+item.getShippingCost());
		}
	}
	
}
