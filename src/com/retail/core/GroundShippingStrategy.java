/**
 * 
 */
package com.retail.core;

import java.text.DecimalFormat;

/**
 * @author gyelwand
 *
 */
public class GroundShippingStrategy implements ShippingStrategy {
	
	//This method is used to calculate the shipping cost of Item for Ground method type
	public void calculateShippingCost(Item item) {
		
		double itemWeight=item.getWeight();
		
		double shippingCost=(itemWeight * 2.5);
		
		DecimalFormat f = new DecimalFormat("##.##");
		String shippingCostStr=f.format(shippingCost);
		 
		shippingCost=Double.parseDouble(shippingCostStr);
		
		item.setShippingCost(shippingCost);
	}

}
