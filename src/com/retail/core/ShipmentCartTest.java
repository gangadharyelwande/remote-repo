/**
 * 
 */
package com.retail.core;

/**
 * @author gyelwand
 *
 */
public class ShipmentCartTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Create the item object
		Item itemObject1= new Item("567321101987","CD � Pink Floyd, Dark Side Of The Moo",19.99,0.58, "AIR");
		Item itemObject2= new Item("567321101986","CD � Beatles, Abbey Road",17.99,0.61, "GROUND");
		Item itemObject3= new Item("567321101985","CD � CD � Queen, A Night at the Opera",20.49,0.55, "AIR");
		Item itemObject4= new Item("567321101984","CD � Michael Jackson, Thriller",23.88,0.50, "GROUND");
		Item itemObject5= new Item("467321101899","iPhone - Waterproof Case",9.75,0.73, "AIR");
		Item itemObject6= new Item("477321101878","iPhone -  Headphones",17.25,3.21, "GROUND");

		//instantiate Shipping cart to store items in the list
		ShipmentCart shippingCartObject= new ShipmentCart();
		
		//add items in the list
		shippingCartObject.addItem(itemObject1);
		shippingCartObject.addItem(itemObject2);
		shippingCartObject.addItem(itemObject3);
		shippingCartObject.addItem(itemObject4);
		shippingCartObject.addItem(itemObject5);
		shippingCartObject.addItem(itemObject6);

		//display the Shipment report
		shippingCartObject.display();
		System.out.println();
		System.out.println("************************************************************************************************************************************************");
		System.out.println("Display items in the key value pair\n");
		
		//display Items in another way
		shippingCartObject.displayItemsWithColumnNameValuePair();
		
	}

}
